MAVSDK-TypeScript
=================

TypeScript client library for [MAVSDK](https://github.com/mavlink/MAVSDK) gRPC.

> Inspired by [MAVSDK-JavaScript](https://github.com/mavlink/MAVSDK-JavaScript) and intended to further develop this POC
> into a production-ready library.

Development
-----------

Initialise project:

```shell
make init
```

Build Docker images and generate web-gRPC bindings:

```shell
make build
```

Optionally install dependencies and build for development on host machine:

```shell
make build-local
```

### Testing

Start dependencies (wait 10-20 seconds or use QGroundControl connected to UDP `14550` to test SITL availability):

```shell
make deps
```

Run tests:

```shell
make test
```

Clean up dependencies and temporary files (except `.env`):

```shell
make clean
```
