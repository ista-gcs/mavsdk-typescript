import {TelemetryServiceClient} from '../../src/mavsdk/gRPC/telemetry/TelemetryServiceClientPb';
import {
    SubscribeHeadingRequest
} from '../../src/mavsdk/gRPC/telemetry/telemetry_pb';
import {API_HOST} from "../settings";

beforeAll(() => {
    require('browser-env')();
});

const CONNECTION_URLS = [
    `${API_HOST}/api/v1/drones/0`,
];

describe('Basic', () => {
    it('should communicate with all MAVLink connections', (done) => {
        let successCount = 0;

        CONNECTION_URLS.forEach((url) => {
            const service = new TelemetryServiceClient(url);
            const request = new SubscribeHeadingRequest();

            const stream = service.subscribeHeading(request);

            stream.on('data', (response) => {
                expect(response.getHeadingDeg()).toBeDefined();
                stream.cancel();
                checkSuccess();
            });
        });

        const checkSuccess = () => {
            successCount++;
            if (successCount == CONNECTION_URLS.length) {
                done();
            }
        };
    });
});
