import {TelemetryServiceClient} from '../../src/mavsdk/gRPC/telemetry/TelemetryServiceClientPb';
import {
    SubscribeAttitudeEulerRequest,
    SubscribeCameraAttitudeEulerRequest,
    SubscribePositionRequest
} from '../../src/mavsdk/gRPC/telemetry/telemetry_pb';
import {API_HOST} from "../settings";

const CONNECTION_URL = `${API_HOST}/api/v1/drones/0`

beforeAll(() => {
    require('browser-env')();
});

describe('Telemetry', () => {
    it('should track position', (done) => {
        const service = new TelemetryServiceClient(CONNECTION_URL)
        const request = new SubscribePositionRequest();

        const stream = service.subscribePosition(request);

        stream.on('data', (response) => {
            expect(response.getPosition()).toBeDefined();
            stream.cancel();
            done();
        });
    });

    it('should track attitude in Euler angels', (done) => {
        const service = new TelemetryServiceClient(CONNECTION_URL)
        const request = new SubscribeAttitudeEulerRequest();

        const stream = service.subscribeAttitudeEuler(request);

        stream.on('data', (response) => {
            expect(response.getAttitudeEuler()).toBeDefined();
            stream.cancel();
            done();
        });
    });

    it('should track camera attitude in Euler angels', (done) => {
        const service = new TelemetryServiceClient(CONNECTION_URL)
        const request = new SubscribeCameraAttitudeEulerRequest();

        const stream = service.subscribeCameraAttitudeEuler(request);

        stream.on('data', (response) => {
            expect(response.getAttitudeEuler()).toBeDefined();
            stream.cancel();
            done();
        });
    });
});
