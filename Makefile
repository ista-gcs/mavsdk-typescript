###########################################################
# Developer's Guide
###########################################################
#
# All tasks should be explicitly marked as .PHONY at the
# top of the section.
#
# We distinguish two types of tasks: private and public.
#
# "Public" tasks should be created with the description
# using ## comment format:
#
#   public-task: task-dependency ## Task description
#
# Private tasks should start with "_". There should be no
# description E.g.:
#
#   _private-task: task-dependency
#

###########################################################
# Setup
###########################################################

# Include .env file
ifneq (,$(wildcard ./.env))
    include .env
    export
endif

###########################################################
# Project directories
###########################################################

root_dir := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
grpc_dir := $(root_dir)/src/mavsdk/gRPC

###########################################################
# Config
###########################################################

dotenv_paths := "$(root_dir)"
version := $(shell git describe --tags --abbrev=0 2>/dev/null || echo UNKNOWN)

###########################################################
# Help
###########################################################
.PHONY: help

help: ## Shows help
	@printf "\033[33m%s:\033[0m\n" 'Use: make <command> where <command> one of the following'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[32m%-18s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

info: ## Prints project info
	@echo "Version: $(version)"

###########################################################
# Initialization
###########################################################
.PHONY: init init-env init-submodules

init: init-env init-submodules

init-env: ## Initializes .env files
	@echo "Creating .env files in $(dotenv_paths)..." && $(foreach dir, $(dotenv_paths), rsync -a -v --ignore-existing $(dir)/.env.template $(dir)/.env; )

reset-env: ## Resets .env files
	@echo "Deleting .env files in $(dotenv_paths)..." && $(foreach dir, $(dotenv_paths), rm -f $(dir)/.env; )

init-submodules: ## Init submodules
	@echo "Initialising submodules" git submodule update --init --recursive

pull: ## Pulls Docker images
	@docker-compose pull

###########################################################
# Building
###########################################################
.PHONY: build build-docker generate install

build: ## Build all services in Docker
	@docker-compose build

build-local: generate ## Build locally

generate: install ## Build all services in Docker
	@npm run generate

install: ## Install local dependencies
	@npm install

###########################################################
# Running
###########################################################
.PHONY: up down deps

up: ## Starts all services in Docker
	@docker-compose up

deps: ## Starts all dependencies in detached mode
	@docker-compose up -d api

down: ## Shuts down dockerized application and removes docker resources
	@docker-compose down --remove-orphans

###########################################################
# Testing
###########################################################
.PHONY: test

test: ## Run tests
	@docker-compose run --rm main test

test-local: ## Run tests locally
	@npm run test

###########################################################
# Cleaning
###########################################################
.PHONY: clean clean-generated clean-nodejs

clean: down clean-generated clean-nodejs ## Cleans environment

clean-generated: ## Clean generated code
	@rm -rf $(grpc_dir)

clean-nodejs: ## Clean Node.js dependencies
	@rm -rf $(root_dir)/node_modules
