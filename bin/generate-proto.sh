#!/usr/bin/env bash

set -e

function info-section {
   echo "-----------------------------------------------------------"
   echo -e "$1"
   echo "-----------------------------------------------------------"
}

###############################################################################
# Setup
###############################################################################

script_dir="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )"
root_dir="$(dirname "${script_dir}")"
src_dir="${root_dir}/src"
output_dir="${src_dir}/mavsdk/gRPC"
proto_dir="${root_dir}/proto/protos"

protos=$(find "${proto_dir}" -name "*.proto" -type f)

js_out_import_style="commonjs,binary"
grpc_web_out_import_style="typescript"
grpc_mode="grpcwebtext" # Should we use 'grpcweb' instead?

###############################################################################
# Functions
###############################################################################

function generate() {
  pushd "${root_dir}" > /dev/null

  info-section "Generating MAVSDK...
  Destination: ${output_dir}"

  rm -rf "${output_dir}"
  mkdir -p "${output_dir}"

  for proto_file in $protos; do
    module_name=$(basename -- "${proto_file}" | cut -f 1 -d '.')
    echo "  [+] Generating: ${module_name}"

    # Check TypeScript workaround: https://github.com/grpc/grpc-web#typescript-support
    protoc \
      -I="${proto_dir}" \
      --js_out=import_style="${js_out_import_style}":"${output_dir}" \
      --grpc-web_out=import_style="${grpc_web_out_import_style}",mode="${grpc_mode}":"${output_dir}" \
      "${proto_file}"
  done

  echo "  [+] Done"

  popd > /dev/null
}

###############################################################################
# Main
###############################################################################

generate
